function isValidEmail(email) {
    if (email === '') {
        return false;
    }
    const emailRegex = /^(?!.*\.{2})[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  
    if (!emailRegex.test(email)) {
      return false;
    }
  
    const [localPart, domain] = email.split('@');
  
    if (localPart.length > 64) {
      return false;
    }
  
    if (domain.length > 255) {
      return false;
    }
  
    if (!domain.includes('.')) {
      return false;
    }
  
    if (domain.endsWith('.')) {
      return false;
    }
  
    const domainParts = domain.split('.');
    for (const part of domainParts) {
      if (part.length > 63) {
        return false;
      }
      if (!/^[a-zA-Z0-9-]+$/.test(part)) {
        return false;
      }
      if (part.startsWith('-') || part.endsWith('-')) {
        return false;
      }
    }
  
    return true;
  }


document.addEventListener('DOMContentLoaded', () => {
    const apiHostname = "https://xevp7k6f03.execute-api.eu-central-1.amazonaws.com";

    const emailInput = document.getElementById('newsletter--input-email');
    const subscribeButton = document.getElementById('newsletter--button-subscribe');

    function validateEmail() {
        const email = emailInput.value.trim();
        if (isValidEmail(email)) {
            subscribeButton.disabled = false;
        } else {
            subscribeButton.disabled = true;
        }
    }

    function subscribe() {
        validateEmail();
        if (subscribeButton.disabled) {
            return;
        }

        const email = emailInput.value.trim();

        subscribeButton.classList.add('loading');
        subscribeButton.disabled = true;

        fetch(apiHostname + '/website/v1/newsletter/subscription', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({ email: email }),
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
        })
        .then(data => {
            handleSuccess();
        })
        .catch(error => {
            handleFailure();
        });
    }

    emailInput.addEventListener('input', validateEmail);
    validateEmail();

    // TODO: double Enter should not send two requests
    emailInput.addEventListener('keypress', (event) => {
        if (event.key === 'Enter') {
            subscribe();
        }
    });

    subscribeButton.addEventListener('click', subscribe);

    function handleSuccess() {
        subscribeButton.classList.remove('loading');
        subscribeButton.classList.add('success');
        subscribeButton.textContent = 'Success';
        emailInput.value = "";
        setTimeout(() => {
            resetButton();
        }, 3000);
    }

    function handleFailure() {
        subscribeButton.classList.remove('loading');
        subscribeButton.classList.add('failure');
        subscribeButton.textContent = 'Failure';
        setTimeout(() => {
            resetButton();
        }, 3000);
    }

    function resetButton() {
        subscribeButton.classList.remove('success', 'failure', 'loading');
        subscribeButton.textContent = 'Subscribe';
        validateEmail();
    }
});
