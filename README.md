## Domain

Purchased and managed on Namecheap.

## Hosting

Hosted for free on Gitlab Pages.

## Deployment

Deployed by CI pipeline when `main` branch is updated.